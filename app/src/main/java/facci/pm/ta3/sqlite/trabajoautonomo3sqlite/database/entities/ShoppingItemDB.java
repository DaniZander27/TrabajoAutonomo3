package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

import static facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities.ShoppingItemDB.ShoppingElementEntry.COLUMN_NAME_TITLE;
import static facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities.ShoppingItemDB.ShoppingElementEntry.CREATE_TABLE;
import static facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities.ShoppingItemDB.ShoppingElementEntry.TABLE_NAME;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);

    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    // se crea un metodo que devuelva un ContentValues el cual va a generar
    // el contenedor donde se colocara el nuevo elemento a insertar
    public ContentValues generarContenedor(String productName){
        ContentValues valores = new ContentValues(); //Se crea el objeto de tipo ContentValues
        valores.put(COLUMN_NAME_TITLE,productName); //Con el metodo put se hace referencia a la columna nombre de la tabla
    return valores; // regresa el objeto
    }
    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        SQLiteDatabase db = dbHelper.getWritableDatabase(); //se abre la base de datos
        db.insert(TABLE_NAME, null, generarContenedor(productName));// se utiliza el metodo insert que tiene como parametros
                                                                                 // el nombre de la tabla, que hacer si el contenedor esta vacio
                                                                                 // y el tercer parametro el contenedor

        db.close(); // se cierra la base de datos
    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos

        SQLiteDatabase db = dbHelper.getWritableDatabase(); //abrir la base de datos
        db.delete(TABLE_NAME,"", null); //metodo delete que necesita el nombre de la tabla
                                                            //la clausula del where y los argumentos del where
        db.close(); // se cierra la base de datos

        //Otra forma de eliminar todos los elementos
        //SQLiteDatabase db = dbHelper.getWritableDatabase();
        //db.execSQL("Delete from " +TABLE_NAME ); // se puede ejecutar un comando de SQL
        //db.close();
    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos

        SQLiteDatabase db = dbHelper.getWritableDatabase(); //se abre la base de datos
        ContentValues valores = new ContentValues(); // se crea un objeto de tipo ContentValues
        valores.put(COLUMN_NAME_TITLE, shoppingItem.getName()); // se coloca el nuevo nombre al contenedor
        String whereClause = ShoppingElementEntry._ID + " = " + shoppingItem.getId(); // se crea la clausula del where de tipo String
        db.update(TABLE_NAME, valores, whereClause, null); // se usa el metodo update necesita el nombre de la tabla, el contenedor
                                                                    // la clausula where y el argumento del where
        db.close(); // se cierra la base de datos
    }

    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos

        SQLiteDatabase db = dbHelper.getWritableDatabase(); // se abre la base de datos
        String whereClause = ShoppingElementEntry._ID + " = " + shoppingItem.getId(); // se crea la clausula del where
        db.delete(TABLE_NAME, whereClause, null); // se usa el metodo delete que necesita el nombre de la tabla, la clausula where
                                                            // y el argumento del where
        db.close(); // se cierra la base de datos
    }


}
